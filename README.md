# Meet up Cypress
This project was bootstrapped with Create React App.  
It's very simple application to present basic usage of Cypress.

To launch application : `npm start`  
To launch cypress tests :
- Open mode (write/debug tests) : `npm run cypress:open`
- Run mode (ci with screens & videos) : `npm run cypress:run`

Interesting files modified during the demo:
- cypress-meetup/src/App.js
- cypress-meetup/cypress.json
- cypress-meetup/cypress/support/commands.js
- cypress-meetup/cypress/integration/meetup-test.spec.js