describe("Meet up cypress", () => {
    beforeEach(() => {
        cy.visit("")
        cy.login("user", Cypress.env("password"))
    })

    it("Search title + verify text", () => {
        cy.get("[data-cy=title]").contains("Meet up Cypress")
    })

    it("Click button + wait request + verify text", () => {
        cy.intercept("https://jsonplaceholder.typicode.com/users/1").as("alias")
        cy.get("[data-cy=button]").click()
        cy.wait("@alias")
        cy.get("[data-cy=user]").contains("Leanne Graham")
    })

    it("Click button + mock + verify text", () => {
        cy.intercept("https://jsonplaceholder.typicode.com/users/1", {fixture: "meet-up-users.json"})
        cy.get("[data-cy=button]").click()
        cy.get("[data-cy=user]").contains("Meet up")
    })

    it("type text input", () => {
        cy.get("[data-cy=input]").type("Bonjour").clear().type("Coucou")
    })
})




















