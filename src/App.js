import React, {useState} from "react";

export const App = () => {

    const [userName, setUserName] = useState("")
    const getUser = () => {
        fetch("https://jsonplaceholder.typicode.com/users/1")
            .then((response) => response.json())
            .then((json) => setUserName(json.name));
    };


    return (
        <div className="App">
            <h1 data-cy={"title"}>Meet up Cypress</h1>

            <button data-cy={"button"} onClick={() => getUser()} >Get user</button>
            <p>user name : <span data-cy={"user"} >{userName}</span></p>

            <input data-cy={"input"} type="text" name="name"/>

        </div>
    );
}

export default App;
